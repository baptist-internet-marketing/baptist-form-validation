var validate = new Bouncer('form',{
    fieldClass: 'u-borderColorRed',
    errorClass: 'Error',
    patterns: {
		email: /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$/,
		url: /^(?:(?:https?|HTTPS?|ftp|FTP):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/,
		//date: /(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d/
    },
    customValidations: {
		valueMismatch: function (field) {

			// Look for a selector for a field to compare
			// If there isn't one, return false (no error)
			var selector = field.getAttribute('data-bouncer-match');
			if (!selector) return false;

			// Get the field to compare
			var otherField = field.form.querySelector(selector);
			if (!otherField) return false;

			// Compare the two field values
			// We use a negative comparison here because if they do match, the field validates
			// We want to return true for failures, which can be confusing
			return otherField.value !== field.value;

		}
	},
    messageCustom: 'data-bouncer-message',
    messageTarget: 'data-bouncer-target',
    messages: {
		patternMismatch: {
			date: 'Please use the MM-DD-YYYY format',
        },
        valueMismatch: function (field) {
			var customMessage = field.getAttribute('data-bouncer-mismatch-message');
			return customMessage ? customMessage : 'Please make sure the fields match.'
		}
    }
});

const form = document.getElementById('form');

const inputs = form.getElementsByTagName('input');

for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == 'text') {
        inputs[i].onchange = function() {
        this.value = this.value.replace(/^\s+/, '').replace(/\s+$/, '');
        };
    }
    if (inputs[i].type == 'tel') {
        inputs[i].oninput = function(e) {
            var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
            e.target.value = !x[2] ? x[1] : x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
        };
    }
}

