# Baptist Health Form Validation

[View Demo](http://baptistformvalidation.bitbucket.org)

Front-end form validation will be handled by [Bouncer.js](https://github.com/cferdinandi/bouncer), a lightweight Vanilla JS library that uses native HTML5 validation methods.

If you need help defining validation patterns for inputs, check out [HTML 5 Patterns](http://html5pattern.com/).
